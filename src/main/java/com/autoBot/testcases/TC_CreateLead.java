package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC_CreateLead extends Annotations {

	
	@BeforeTest
	public void setData() {
		testcaseName = "TC_CreateLead";
		testcaseDec = "Login and CreateLead";
		author = "nirmala";
		category = "smoke";
		excelFileName = "CreateLeadData";		
	} 
	
	@Test(dataProvider="getData")
	public void login(String username,String password,String CompanyName,String FirstName,String LastName,String VFirstN) {
		new LoginPage()
		.enterUserName(username)
		.enterPassWord(password)
		.clickLogin()
		.clickcrmsfaLink()
		.clickLeadsTab()
		.clickCreateLeadTab()
		.enterCompanyname(CompanyName)
		.enterFirstname(FirstName)
		.enterLastname(LastName)
		.clickCreateLead()
		.verifyFirstname(VFirstN);
	}

}
