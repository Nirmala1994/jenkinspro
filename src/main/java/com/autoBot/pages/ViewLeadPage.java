package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage verifyFirstname(String VerifyFN) {
		WebElement eleFN = locateElement("id","viewLead_firstName_sp");
		verifyExactText(eleFN, VerifyFN);	
		return this;
		
	}

}
