package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLeadTab() {
		WebElement eleCreatetab = locateElement("xpath","//a[text()='Create Lead']");
		click(eleCreatetab);
		return new CreateLeadPage();
	}

}
