package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {
	
	public MyLeadsPage clickLeadsTab() {
		WebElement eleLeadTab = locateElement("link","Leads");
		click(eleLeadTab);
		return new MyLeadsPage();
	}

}
