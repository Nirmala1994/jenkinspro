package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
	public CreateLeadPage enterCompanyname(String CompN) {
		WebElement eleCN = locateElement("id","createLeadForm_companyName");
		clearAndType(eleCN, CompN);
		return this;
	}
	
	public CreateLeadPage enterFirstname(String FirstN) {
		WebElement eleFN = locateElement("id","createLeadForm_firstName");
		clearAndType(eleFN, FirstN);
		return this;
	}
	
	public CreateLeadPage enterLastname(String LastN) {
		WebElement eleLN = locateElement("id","createLeadForm_lastName");
		clearAndType(eleLN, LastN);
		return this;
	}
	
	public ViewLeadPage clickCreateLead() {
		WebElement eleCreatebtn = locateElement("xpath","//input[@value='Create Lead']");
		click(eleCreatebtn);
		return new ViewLeadPage();
	}
	
	
	

}
